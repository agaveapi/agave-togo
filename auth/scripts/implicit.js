// staging
var OAuthClients = {
    "araport.org": {
        "clientKey": "WrFKPGDZd6L7m2PEzTxNGw7YpyAa",
        "callbackUrl": "https://togo.staging.agaveapi.co/auth/",
        "scope": "PRODUCTION",
        "projectUrl": "https://www.araport.org",
        "supportUrl": "https://www.araport.org/contact",
        "signupUrl": "https://www.araport.org/user/register"
    },
    "tacc.prod": {
        "clientKey": "qflYPovh1XYCzrPZKf8mWWH0NtIa",
        "callbackUrl": "https://togo.staging.agaveapi.co/auth/",
        "scope": "PRODUCTION",
        "projectUrl": "https://tacc.utexas.edu/",
        "supportUrl": "https://portal.tacc.utexas.edu/tacc-consulting",
        "signupUrl": "https://portal.tacc.utexas.edu/account-request"
    },
    "agave.prod": {
        "clientKey": "_2xmhvwpwGOLwLqP6AkR9stGHoQa",
        "callbackUrl": "http://localhost:8080/app/",
        "scope": "PRODUCTION",
        "projectUrl": "https://agaveapi.co/",
        "supportUrl": "https://support.agaveapi.co",
        "signupUrl": "https://public.agaveapi.co/create_account"
    },
    // "agave.prod": {
    //     "clientKey": "ApXiZQtdxzaqN_KEWhF6vw9X4PUa",
    //     "callbackUrl": "https://togo.staging.agaveapi.co/auth/",
    //     "scope": "PRODUCTION",
    //     "projectUrl": "https://agaveapi.co/",
    //     "supportUrl": "https://support.agaveapi.co",
    //     "signupUrl": "https://public.agaveapi.co/create_account"
    // },
    // "iplantc.org": {
    //     "clientKey": "GlGcjTWcYXtCzBXAAbi3IQFhjuIa",
    //     "callbackUrl": "http://localhost:9000/auth/",
    //     "scope": "PRODUCTION",
    //     "projectUrl": "https://cyverse.org/",
    //     "supportUrl": "https://ask.cyverse.org/",
    //     "signupUrl": "https://user.cyverse.org/register/"
    // },
    "iplantc.org": {
        "clientKey": "D1qAH1mhYLKiTqep6c_9KUZyLYAa",
        "callbackUrl": "http://localhost:9000/auth/",
        "scope": "PRODUCTION",
        "projectUrl": "https://cyverse.org/",
        "supportUrl": "https://ask.cyverse.org/",
        "signupUrl": "https://user.cyverse.org/register/"
    },
    "designsafe": {
        "clientKey": "oRy7dGF5zLDQdlvrMrLfcCSHZwMa",
        "callbackUrl": "https://togo.staging.agaveapi.co/auth/",
        "scope": "PRODUCTION",
        "projectUrl": "https://designsafe-ci.org/",
        "supportUrl": "https://www.designsafe-ci.org/help/new-ticket/",
        "signupUrl": "https://www.designsafe-ci.org/account/register/"
    },
    "dev.staging": {
        "clientKey": "fSsbB1Lj1W4qKXrDQWSk78gVuE4a",
        "callbackUrl": "https://togo.staging.agaveapi.co/auth/",
        "scope": "PRODUCTION",
        "projectUrl": "https://agaveapi.co",
        "supportUrl": "https://support.agaveapi.co",
        "signupUrl": "https://public.agaveapi.co/create_account"
    },
    "sgci": {
        "clientKey": "8maiQzf82y4Qeg7acRlF3RHP9Cca",
        "callbackUrl": "https://togo.staging.agaveapi.co/auth/",
        "scope": "PRODUCTION",
        "projectUrl": "https://sciencegateways.org",
        "supportUrl": "https://sciencegateways.org/contact-us",
        "signupUrl": "https://sgci.agaveapi.co/create_account"
    }
};

// // production
// var OAuthClients = {
//     "araport.org": {
//         "clientKey": "fPf99O2yKHHWUtrNq_CA2DDVemca",
//         "callbackUrl": "https://togo.agaveapi.co/auth/",
//         "createAccountUrl": "https://www.araport.org/user/register",
//         "scope": "PRODUCTION"
//     },
//     "tacc.prod": {
//         "clientKey": "Ih16PehABZcKaU7jLbN0qDVhPTka",
//         "callbackUrl": "https://togo.agaveapi.co/auth/",
//         "createAccountUrl": "https://portal.tacc.utexas.edu/account-request",
//         "scope": "PRODUCTION"
//     },
//     "agave.prod": {
//         "clientKey": "ZczLM77Fhrbsb_zDLpVQ9oo1xwoa",
//         "callbackUrl": "https://togo.agaveapi.co/auth/",
//         "createAccountUrl": "https://public.agaveapi.co/create_account",
//         "scope": "PRODUCTION"
//     },
//     "iplantc.org": {
//         "clientKey": "pGhvTvzlegOlwqfGiQ849iK5M5Ia",
//         "callbackUrl": "https://togo.agaveapi.co/auth/",
//         "createAccountUrl": "https://user.cyverse.org/register/",
//         "scope": "PRODUCTION"
//     },
//     "designsafe": {
//         "clientKey": "GGocfJpzwX21H8Jp8yUv6OydfcIa",
//         "callbackUrl": "https://togo.agaveapi.co/auth/",
//         "createAccountUrl": "https://www.designsafe-ci.org/account/register/",
//         "scope": "PRODUCTION"
//     },
//     "dev.staging": {
//         "clientKey": "T5zzfUqLyZQf25TF8BHo66hKIR8a",
//         "callbackUrl": "https://togo.agaveapi.co/auth/",
//         "createAccountUrl": "https://public.agaveapi.co/create_account",
//         "scope": "PRODUCTION"
//     },
//     "sgci": {
//       "clientKey": "VcmT2bf_ywTDo7qh2xCChUZRGbwa",
//       "callbackUrl": "https://togo.agaveapi.co/auth/",
//       "createAccountUrl": "https://sgci.agaveapi.co/create_account",
//       "scope": "PRODUCTION"
//     }
// };
